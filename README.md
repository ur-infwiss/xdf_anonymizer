XDF anonymizer
==============
This script is intended to remove certain streams from existing xdf recordings.

Installation
------------
This script should run on any operating system that is supported by python. It was tested under Windows using Python 3.7.0.
There are no further dependencies to satisfy.

Usage
-----
A script invocation usually should look something like this:
`python .\xdf_anonymizer.py PATH_OF_THE_RECORDING [PATH_OF_THE_OUTPUT_FILE] [--exclude-by-name LIST_OF_STREAM_NAMES_TO_EXCLUDE]`

Note that the arguments `--exclude-by-name`, `--exclude-by-type` and `--exclude-by-sourceid` have to be followed by a list of Strings
which MAY NOT be comma separated.

Type `python .\xdf_anonymizer.py --help` to get this help page:

    positional arguments:
      srcfile               The path to the input xdf[z] file
      destfile              [optional] The path to the created xdf[z] file. If
                            nothing is specified the destfile will be created in
                            the same folder as the srcfile and a postfix will be
                            added to it's name.
    
    optional arguments:
      -h, --help            show this help message and exit
      -f, --force           Forces the destfile to override existing files. BE
                            CAREFUL!!!
      -idp, --include-damaged-parts
                            If this flag is set parts of the file that are damaged
                            and therefore can't be interpreted will be included.
                            If the flag is not set those parts will be excluded as
                            they may leak information about removed streams.
      -en EXCLUDED_NAMES [EXCLUDED_NAMES ...], --exclude-by-name EXCLUDED_NAMES [EXCLUDED_NAMES ...]
                            Specifies the removed streams by their name.
      -et EXCLUDED_TYPES [EXCLUDED_TYPES ...], --exclude-by-type EXCLUDED_TYPES [EXCLUDED_TYPES ...]
                            Specifies the removed streams by their type.
      -es EXCLUDED_SOURCEID [EXCLUDED_SOURCEID ...], --exclude-by-sourceid EXCLUDED_SOURCEID [EXCLUDED_SOURCEID ...]
                            Specifies the removed streams by their source-id.
      -v, --verbose         Enables verbose output.

Examples
--------
`python xdf_anonymizer.py C:/Users/<USERNAME>/Desktop/FILENAME.xdf C:/Users/<USERNAME>/Desktop/OUT_FILENAME.xdf --exclude-by-name Name1 "Name with spaces" --exclude-by-type Audio --exclude-by-sourceid sourceid123`
This will remove the streams named "Name1" and "Name with spaces", all audio streams and the stream "sourceid123".

`python xdf_anonymizer.py C:/Users/<USERNAME>/Desktop/FILENAME.xdf --exclude-by-name BadStream`
This is a more realistic example removing only the stream "BadStream". As no explicit name for the destination file is
specified it will be named "FILENAME_anonymized.xdf" and be located in the same directory as the input file.