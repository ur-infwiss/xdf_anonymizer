# This script is heavily based on https://github.com/sccn/xdf/blob/master/Python/xdf.py

import argparse
import os
import struct
import sys
import xml.etree.ElementTree as ET
from collections import defaultdict

stream_ids_to_remove = set([])
removed_streams_description = []

input_stream = None
buffered_input_stream = None
output_stream = None


class BufferedFileStream:
    def __init__(self, file_stream):
        self.file_stream = file_stream
        self.file_stream_buffer = None

    def tell(self):
        return self.file_stream.tell()

    def seek(self, offset: int, whence: int = 0):
        return self.file_stream.seek(offset, whence)

    def read(self, byte_count):
        read_bytes = self.file_stream.read(byte_count)

        if self.file_stream_buffer is None:
            self.file_stream_buffer = read_bytes
        else:
            self.file_stream_buffer += read_bytes

        return read_bytes

    def clear_buffer(self):
        return_buffer = self.file_stream_buffer
        self.file_stream_buffer = None
        return return_buffer


def _read_varlen_int(f):
    """Read a variable-length integer."""
    nbytes = struct.unpack('B', f.read(1))[0]
    if nbytes == 1:
        return struct.unpack('B', f.read(1))[0]
    elif nbytes == 4:
        return struct.unpack('<I', f.read(4))[0]
    elif nbytes == 8:
        return struct.unpack('<Q', f.read(8))[0]
    else:
        raise RuntimeError('invalid variable-length integer encountered.')


def _xml2dict(t):
    """Convert an attribute-less etree.Element into a dict."""
    dd = defaultdict(list)
    for dc in map(_xml2dict, list(t)):
        for k, v in dc.items():
            dd[k].append(v)
    return {t.tag: dd or t.text}


def _scan_forward(f):
    """Scan forward through the given file object until after the next
    boundary chunk."""
    blocklen = 2 ** 20
    signature = bytes([0x43, 0xA5, 0x46, 0xDC, 0xCB, 0xF5, 0x41, 0x0F,
                       0xB3, 0x0E, 0xD5, 0x46, 0x73, 0x83, 0xCB, 0xE4])
    while True:
        curpos = f.tell()
        block = f.read(blocklen)
        matchpos = block.find(signature)
        if matchpos != -1:
            f.seek(curpos + matchpos + 15)
            print('  scan forward found a boundary chunk.')
            return block
        if len(block) < blocklen:
            print('  scan forward reached end of file with no match.')
            return block


def write_buffer_to_file(stream_id=None):
    buffer_content = buffered_input_stream.clear_buffer()
    if buffer_content is None:
        return
    if stream_id is not None and stream_id in stream_ids_to_remove:
        return
    output_stream.write(buffer_content)


def write_damaged_buffer_to_file():
    print('\tThis means that this file is damaged and a section of it\'s content can not be interpreted.')

    if include_damaged_parts:
        print('\tThis section will be included in the resulting file because the --include-damaged-parts '
              'flag is set.')
        write_buffer_to_file()
    else:
        print('\tThis section will be removed from the file to ensure not to leak any data. To change this '
              'set the --include-damaged-parts flag.')
        buffered_input_stream.clear_buffer()


# main program

parser = argparse.ArgumentParser(description='Anonymizes the recording by removing certain streams.')
parser.add_argument('srcfile', type=str, help='The path to the input xdf[z] file')
parser.add_argument('destfile', type=str, nargs='?', default=None,
                    help='[optional] The path to the created xdf[z] file. If nothing is specified the destfile will '
                         'be created in the same folder as the srcfile and a postfix will be added to it\'s name.')
parser.add_argument('-f', '--force', action='store_true', dest='overwrite_file',
                    help='Forces the destfile to override existing files. BE CAREFUL!!!')
parser.add_argument('-idp', '--include-damaged-parts', action='store_true', dest='include_damaged_parts',
                    help='If this flag is set parts of the file that are damaged and therefore can\'t be '
                         'interpreted will be included. If the flag is not set those parts will be excluded as they '
                         'may leak information about removed streams.')
parser.add_argument('-en', '--exclude-by-name', dest='excluded_names', type=str, nargs='+',
                    default=[], help='Specifies the removed streams by their name.')
parser.add_argument('-et', '--exclude-by-type', dest='excluded_types', type=str, nargs='+',
                    default=[], help='Specifies the removed streams by their type.')
parser.add_argument('-es', '--exclude-by-sourceid', dest='excluded_sourceid', type=str, nargs='+',
                    default=[], help='Specifies the removed streams by their source-id.')
parser.add_argument('-eu', '--exclude-by-uid', dest='excluded_uid', type=str, nargs='+',
                    default=[], help='Specifies the removed streams by their uid.')
parser.add_argument('-v', '--verbose', action='store_true', dest='verbose',
                    help='Enables verbose output.')

args = parser.parse_args()
src_file_string = args.srcfile
dest_file_string = args.destfile
if dest_file_string is None:
    # https://stackoverflow.com/questions/541390/extracting-extension-from-filename-in-python
    src_filename, src_file_extension = os.path.splitext(src_file_string)
    dest_file_string = src_filename + '_anonymized' + src_file_extension
overwrite_file = args.overwrite_file

include_damaged_parts = args.include_damaged_parts

excluded_streams_name = args.excluded_names
excluded_streams_type = args.excluded_types
excluded_streams_sourceid = args.excluded_sourceid
excluded_streams_uid = args.excluded_uid

verbose = args.verbose

if verbose:
    print('Importing XDF file %s...' % src_file_string)

if not os.path.exists(src_file_string):
    sys.exit('file %s does not exist.' % src_file_string)

if not overwrite_file and os.path.exists(dest_file_string):
    sys.exit('The destfile \'' + dest_file_string + '\' already exists. Choose another name or set the --force flag.')

# number of bytes in the file for fault tolerance
filesize = os.path.getsize(src_file_string)

# read file contents ([SomeText] below refers to items in the XDF Spec)
with open(src_file_string, 'rb') as input_stream, open(dest_file_string, 'wb+') as output_stream:
    buffered_input_stream = BufferedFileStream(input_stream)

    # read [MagicCode]
    if buffered_input_stream.read(4) != b'XDF:':
        raise Exception('not a valid XDF file: %s' % src_file_string)

    write_buffer_to_file()

    # for each chunk...
    while True:

        # noinspection PyBroadException
        try:
            # read [NumLengthBytes], [Length]
            chunklen = _read_varlen_int(buffered_input_stream)
        except Exception as e:
            if buffered_input_stream.tell() < filesize - 1024:
                _scan_forward(buffered_input_stream)
                print('got zero-length chunk, scanning forward to next '
                      'boundary chunk.')
                write_damaged_buffer_to_file()
                continue

            else:
                if verbose:
                    print('  reached end of file.')
                break

        # read [Tag]
        tag = struct.unpack('<H', buffered_input_stream.read(2))[0]
        if verbose:
            print('  read tag: %i at %d bytes, length=%d'
                  % (tag, buffered_input_stream.tell(), chunklen))

        # read the chunk's [Content]...
        if tag == 1:
            # read [FileHeader] chunk
            xml_string = buffered_input_stream.read(chunklen - 2)
            write_buffer_to_file()

        elif tag == 2:
            # read [StreamHeader] chunk...
            # read [StreamId]
            stream_id = struct.unpack('<I', buffered_input_stream.read(4))[0]
            # read [Content]
            xml_string = buffered_input_stream.read(chunklen - 6)
            xml = _xml2dict(ET.fromstring(xml_string))
            if verbose:
                print('  found stream ' + xml['info']['name'][0])
                print('  info: ', xml['info'])

            if xml['info']['name'][0] in excluded_streams_name:
                stream_ids_to_remove.add(stream_id)
            if xml['info']['type'][0] in excluded_streams_type:
                stream_ids_to_remove.add(stream_id)
            if xml['info']['source_id'][0] in excluded_streams_sourceid:
                stream_ids_to_remove.add(stream_id)
            if xml['info']['uid'][0] in excluded_streams_uid:
                stream_ids_to_remove.add(stream_id)

            if stream_id in stream_ids_to_remove:
                print('Removing Stream \'' + xml['info']['name'][0] + '\'')

            write_buffer_to_file(stream_id)

        elif tag == 3:
            # read [Samples] chunk...
            try:
                # read [StreamId]
                stream_id = struct.unpack('<I', buffered_input_stream.read(4))[0]
                buffered_input_stream.read(chunklen - 6)

                write_buffer_to_file(stream_id)
            except Exception as e:
                # an error occurred (perhaps a chopped-off file): emit a
                # warning and scan forward to the next recognized chunk
                _scan_forward(buffered_input_stream)
                print('  got error (%s), scanning forward to next '
                      'boundary chunk.', e)
                write_damaged_buffer_to_file()

        elif tag == 6:
            # read [StreamFooter] chunk
            stream_id = struct.unpack('<I', buffered_input_stream.read(4))[0]
            xml_string = buffered_input_stream.read(chunklen - 6)

            write_buffer_to_file(stream_id)

        elif tag == 4:
            # read [ClockOffset] chunk
            stream_id = struct.unpack('<I', buffered_input_stream.read(4))[0]
            buffered_input_stream.read(16)

            write_buffer_to_file(stream_id)

        else:
            # skip other chunk types (Boundary, ...)
            buffered_input_stream.read(chunklen - 2)

            write_buffer_to_file()
